//
//  Color.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class Color: UIColor {
    //计算属性不在内存中存储值，减少内存开销，同时也方便控制取值权限（存储属性也可以控制权限）
    static var main: UIColor {
        get {
            return UIColor(hex: "#5B6DE9")
        }
    }

    static var mainButtonDisabledColor: UIColor {
        get {
            return main.withAlphaComponent(0.5)
        }
    }
    /*
     c7-背景色
     */
    static var backgroundColor: UIColor {
        get {
            return UIColor(hex: "#F5F5F7")
        }
    }
    /*
     用于重要级文字信息、内页标题等主要文字;
     如导航名称、大板块标题、输入后文字
     */
    static var color_45_C2: UIColor {
        get {
            return UIColor(hex: "#454545")
        }
    }
    /*
     用于重要级文字信息、内页标题信息;
     如借款详情已完成的状态、列表正文色等
     */
    static var color_66_C3: UIColor {
        get {
            return UIColor(hex: "#666666")
        }
    }
    /*
     辅助色、用于部分文字强调等;
     如button下面提示文字、借款记录时间等
     */
    static var color_8D_C4: UIColor {
        get {
            return UIColor(hex: "#8D8D8D")
        }
    }
    /*
     提示性、待输入状态、时间等文字;
     如借款详情页还款状态、注册登录填写资料未输入信息等
     */
    static var color_AD_C5: UIColor {
        get {
            return UIColor(hex: "#ADADAD")
        }
    }
    /*
     用于分割线、标签描边
     */
    static var color_DC_C6: UIColor {
        get {
            return UIColor(hex: "#DCDCDC")
        }
    }
    /*
     用于线框描边
     */
    static var color_CC_C8: UIColor {
        get {
            return UIColor(hex: "#CCCCCC")
        }
    }
}
