//
//  DeviceInfo.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/10.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Device

class DeviceInfo: NSObject {

    static func deviceName() ->String {
        return String(describing: Device.version())
    }
    
    static func bundleID() -> String {
        return Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
    }
    
    static func appVersion() -> String {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    }
    
    static func deviceID() -> String {
        let currentDeviceUUIDStr = SAMKeychain.password(forService: DeviceInfo.bundleID(), account: "uuid");
        guard let deviceUUIDStr = currentDeviceUUIDStr else {
            let currentDeviceUUID = UIDevice.current.identifierForVendor
            var deviceUUIDStr = currentDeviceUUID?.uuidString
            deviceUUIDStr = deviceUUIDStr?.replacingOccurrences(of: "-", with: "");
            deviceUUIDStr = deviceUUIDStr?.lowercased()
            SAMKeychain.setPassword(deviceUUIDStr!, forService: DeviceInfo.bundleID(), account: "uuid")
            return deviceUUIDStr!;
        }
        return deviceUUIDStr;
    }
    
}
