//
//  Tools.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class Tools: NSObject {
    //单例创建
    static let `default`: Tools = Tools()
    
    /**
     *返回值是该字符串所占的大小(width, height)
     *font : 该字符串所用的字体(字体大小不一样,显示出来的面积也不同)
     *maxSize :
     为限制改字体的最大宽和高(如果显示一行,则宽高都设置为MAXFLOAT,
     如果显示为多行,只需将宽设置一个有限定长值,高设置为MAXFLOAT)
     */
    //MARK:返回值是该字符串所占的大小(width, height)
    func sizeWithFont(font: UIFont, width: CGFloat, text: String) -> CGSize {
        let attrs:NSDictionary = [kCTFontAttributeName : font]
        let size = CGSize.init(width: width, height: CGFloat(MAXFLOAT))
        return text.boundingRect(with: size, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attrs as? [NSAttributedStringKey : Any], context: nil).size
    }
    
    //获取当前时间
    func getDate() -> String {
        let dateFormat = DateFormatter.init()
        dateFormat.dateFormat = "yyyy.MM.dd"
        let date = NSDate.init()
        let dateString = dateFormat.string(from: date as Date)
        return dateString
    }
    
    /**
     text：正则表达式
     matcherStr:需要进行判断的字符串
     */
    func isContain(text: NSString, matcherStr: String) -> Bool {
        let mailPattern = text
        let matcher = Regex(mailPattern as String)
        if matcher.match(input: matcherStr) {
            return true
        }
        return false
    }
    
    //MARK: 正则--->密码的强度必须是包含大小写字母和数字的组合，不能使用特殊字符，长度在6,13之间。
    func containsPassword(password: String) -> Bool {
        return isContain(text: "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,13}$", matcherStr: password)
    }
    
    //MARK: 正则--->字符串仅能是中文。
    func containsChineseCharacter(text: String) -> Bool{
        return isContain(text:"^[\\u4e00-\\u9fa5]{0,}$", matcherStr: text)
    }
    
    
    //MARK: 正则--->身份证判断
    func isIDValidate(cardNum: String) -> Bool {
        return isContain(text:"^(\\d{14}|\\d{17})(\\d|[xX])$", matcherStr: cardNum)
    }
    
    //MARK: 正则--->手机号判断
    func isCellPhoneValidate(cellPhoneNum: String) -> Bool {
        /**
         * 手机号码:
         * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[6, 7, 8], 18[0-9], 170[0-9]
         * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
         * 联通号段: 130,131,132,155,156,185,186,145,176,1709
         * 电信号段: 133,153,180,181,189,177,1700
         */
        let MOBILE = isContain(text: "^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|7[0678])\\d{8}$", matcherStr: cellPhoneNum)
        
        /**
         * 中国移动：China Mobile
         * 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
         */
        let CM = isContain(text:"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)", matcherStr: cellPhoneNum
        )
        /**
         * 中国联通：China Unicom
         * 130,131,132,155,156,185,186,145,176,1709
         */
        let CU = isContain(text:"(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)", matcherStr: cellPhoneNum)
        
        /**
         * 中国电信：China Telecom
         * 133,153,180,181,189,177,1700
         */
        let CT = isContain(text: "(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)", matcherStr: cellPhoneNum)
        
        /**
         25         * 大陆地区固话及小灵通
         26         * 区号：010,020,021,022,023,024,025,027,028,029
         27         * 号码：七位或八位
         28         */
        let PHS = isContain(text: "^(0\\d{2}-\\d{8}(-\\d{1,4})?)|(0\\d{3}-\\d{7,8}(-\\d{1,4})?)$", matcherStr: cellPhoneNum)
        
        if  MOBILE==true ||
            CM==true ||
            CU==true ||
            CT==true ||
            PHS==true {
            return true
        }else {
            return false
        }
    }
}

//正则匹配
struct Regex {
    let regex: NSRegularExpression?
    
    init(_ pattern: String) {
        regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
    }
    
    func match(input: String) -> Bool {
        if let matches = regex?.matches(in: input, options: [], range: NSMakeRange(0, (input as NSString).length)) {
            return matches.count > 0
        }
        return false
    }
}

