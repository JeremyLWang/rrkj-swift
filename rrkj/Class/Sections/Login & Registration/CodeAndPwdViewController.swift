//
//  CodeAndPwdViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class CodeAndPwdViewController: BaseViewController {
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var pwdView: UIView!
    @IBOutlet weak var mainButton: Button!
    
    lazy var codeInputView = InputView.input
    lazy var pwdInputView = InputView.input
    
    var phoneNum: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideNavigationBarLine()
        self.view.backgroundColor = UIColor.white
        setupInputZone()
        
        self.mainButton.onClick = { [weak self] button in
            self?.registAction()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CodeButton.timerCountDown(forButton: codeInputView.codeButton, title: "秒") { }
    }

}

private extension CodeAndPwdViewController {
    func setupInputZone() {
        
        codeInputView.inputType = InputType.code
        codeInputView.secureButton.removeFromSuperview()
        codeInputView.codeButton.codeAction = { [weak self] button in
            return self?.phoneNum
        }
        codeView.addSubview(codeInputView)
        
        pwdInputView.inputType = InputType.loginPwd
        pwdInputView.textField.placeholder = "请设置6~16位登录密码"
        pwdInputView.codeButton.removeFromSuperview()
        pwdView.addSubview(pwdInputView)
    }
}

//MARK:
private extension CodeAndPwdViewController {
    func registAction() {
        guard let phone = self.phoneNum, !phone.isEmpty else {
            return
        }
        guard let code = codeInputView.textField.text, !code.isEmpty else {
            return
        }
        guard let password = pwdInputView.textField.text, !password.isEmpty else {
            return
        }
        
        let dict = ["phone": phone,
                    "code": code,
                    "password":password,
                    "source":"21"] //后台修改  14 ->21
        HTTPManager.session.postRequest(forKey: kKDZhuceKey, param: dict, succeed: { (json, code, unwrap) in
            if code == 0 && unwrap {
                guard let user = UserModel(JSON: json) else { return }
                
                UserManager.default.loginSuccessToUpdateUserInfo(userModel: user)
                
//                self.navigationController?.view.makeToast("注册成功", duration: 2.0, position: .center) { didTap in
//                    if !didTap {
//                        print("completion from tap")
//                    } else {
//                        print("completion without tap")
//                        self.navigationController?.popToRootViewController(animated: false)
//                        LoginViewController.default.loginSuccess()
//                    }
//                }
                
                tapToast(text: "注册成功", action: { (didTap) in
                    self.navigationController?.popToRootViewController(animated: false)
                    LoginViewController.default.loginSuccess()
                })
            }
        }) { (errMsg) in
            
        }
    }
}














