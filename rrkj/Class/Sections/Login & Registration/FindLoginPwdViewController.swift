//
//  FindLoginPwdViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class FindLoginPwdViewController: BaseViewController {
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var IDView: UIView!
    @IBOutlet weak var codeView: UIView!
    
    @IBOutlet weak var nextButton: Button!
    @IBOutlet weak var botConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var containerView: UIView!
    private lazy var inputView1 = InputView.input
    
    var phoneNum: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideNavigationBarLine()
        self.view.backgroundColor = UIColor.white
        setupInputZone()
        
//        delay(10) {
//            self.middleView.removeFromSuperview()
//        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        self.containerView.addGestureRecognizer(tap)
    }
    
    @objc func tapAction() {
        self.containerView.endEditing(true)
    }
}


private extension FindLoginPwdViewController {
    func setupInputZone() {
        
        inputView1.removeAllButtons()
        inputView1.textField.placeholder = ""
        inputView1.textField.text = self.phoneNum
        inputView1.isUserInteractionEnabled = false
        phoneView.addSubview(inputView1)
        
        let inputView2 = InputView.input
        inputView2.inputType = InputType.name
        inputView2.removeAllButtons()
        nameView.addSubview(inputView2)
        
        let inputView3 = InputView.input
        inputView3.inputType = InputType.IDNum
        inputView3.removeAllButtons()
        IDView.addSubview(inputView3)
        
        let inputView4 = InputView.input
        inputView4.inputType = InputType.code
        inputView4.secureButton.removeFromSuperview()
        inputView4.codeButton.codeAction = { [weak self] button in
            return self?.phoneNum
        }
        codeView.addSubview(inputView4)
    }
}
