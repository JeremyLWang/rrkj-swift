//
//  LoginViewController.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

enum SighInType {
    case quickLogin
    case pwdLogin
}

class LoginViewController: SwipeViewController {
    
    lazy var quickLoginVc: QuickLoginViewController = {
        let quickLoginVc = QuickLoginViewController()
        quickLoginVc.title = "快捷登录"
        quickLoginVc.view.backgroundColor = Color.backgroundColor
        return quickLoginVc
    }()
    
    lazy var pwdLoginVc: PwdLoginViewController = {
        let pwdLoginVc = PwdLoginViewController()
        pwdLoginVc.title = "账号密码登录"
        pwdLoginVc.view.backgroundColor = Color.backgroundColor
        return pwdLoginVc
    }()
    
    static let `default`: LoginViewController = {
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        let navigationController = LoginViewController(rootViewController: pageController)
        navigationController.setFirstViewController(0)
        return navigationController
    }()
    
    public static func showLoginPage() -> LoginViewController {
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        let navigationController = LoginViewController(rootViewController: pageController)
        navigationController.setFirstViewController(0)
        return navigationController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationTitle = "登录"
        self.setupLoginControllers()
        
        quickLoginVc.loginButton.onClick = { (button) in
            self.loginAction(button, type: .quickLogin)
        }
        
        pwdLoginVc.loginButton.onClick = { (button) in
            self.loginAction(button, type: .pwdLogin)
        } 
    }
    
    func loginSuccess() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: view
private extension LoginViewController {
    func setupLoginControllers() {
        setViewControllerArray([quickLoginVc, pwdLoginVc])
        setSelectionBar(23, height: 2, color: Color.main)
        setButtonsWithSelectedColor(UIFont.systemFont(ofSize: 16),
                                    color: Color.color_66_C3,
                                    selectedColor: Color.main)
        equalSpaces = false
    }
}

//MARK: network
private extension LoginViewController {
    func loginAction(_ sender: UIButton, type: SighInType) {
        
        var dict: Dictionary<String, Any> = [:]
        var key = ""
        switch type {
        case .quickLogin:
            guard let userName = self.quickLoginVc.phoneNumText, !userName.isEmpty else {return}
            guard let code = self.quickLoginVc.codeText, !code.isEmpty else { return }
            dict = ["username": userName,
                    "code": code]
            key = kUserQuickLoginBySms
        case .pwdLogin:
            guard let userName = self.pwdLoginVc.phoneNumText, !userName.isEmpty else { return }
            guard let password = self.pwdLoginVc.pwdText, !password.isEmpty else { return }
            dict = ["username": userName,
                    "password": password]
            key = kKDLoginKey
        }
        
        HTTPManager.session.postRequest(forKey: key, param: dict, succeed: { (json, code, unwrapNormal) in
            if code == 0 && unwrapNormal {
                DLog(json)
                guard let user = UserModel(JSON: json) else { return }
                
                UserManager.default.loginSuccessToUpdateUserInfo(userModel: user)
                
                self.loginSuccess()
                
            }
        }) { (errMsg) in
            
        }
    }
}















