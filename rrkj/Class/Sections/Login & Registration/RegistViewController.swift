//
//  RegistViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class RegistViewController: BaseViewController {

    @IBOutlet weak var inputField: UIView!
    @IBOutlet weak var mainButton: Button!
    private lazy var inputView1 = InputView.input
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideNavigationBarLine()
        self.view.backgroundColor = UIColor.white

        inputView1.inputType = InputType.phoneNum
        inputView1.removeAllButtons()
        self.inputField.addSubview(inputView1)
        
        self.mainButton.onClick = { [weak self] button in
            guard let strongSelf = self else { return }
            let next = CodeAndPwdViewController()
            next.phoneNum = strongSelf.inputView1.textField.text
            if let phone = next.phoneNum, phone.count == 11 {
                CodeSecure.md5Encrypt(phoneNum: phone, encryptedCallBack: { (sign, random) in
                    let dict = ["phone": phone,
                                "random": random,
                                "sign": sign]
                    strongSelf.checkRegistStatus(param: dict, check: {
                        strongSelf.navigationController?.pushViewController(next, animated: true)
                    })
                })
            } else {
                toastShow(text: "手机号码应该是合法的11位数字")
            }
        }
    }
}


private extension RegistViewController {
    func checkRegistStatus(param: Dictionary<String, Any> , check: @escaping()->Void) {

        HTTPManager.session.postRequest(forKey: KUserRegGetCode, param: param, succeed: { (json, code, unwrap) in
            DLog(json)
            if code == 0 && unwrap {
                check()
            }
           else if code == 1000 {  //老用户，弹窗
                
            }
        }) { (errMsg) in
            
        }
    }
}
