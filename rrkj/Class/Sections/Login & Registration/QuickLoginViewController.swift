//
//  QuickLoginViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class QuickLoginViewController: BaseViewController {
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var loginButton: Button!
    @IBOutlet weak var codeView: UIView!
    private lazy var inputView1 = InputView.input
    private lazy var inputView2 = InputView.input
    var buttonCanClick: Bool {
        get {
            if (inputView1.textField.text?.count == 11 && inputView2.textField.text?.count == 6) {
                loginButton.backgroundColor = Color.main
                loginButton.isEnabled = true
                return true
            }
            loginButton.backgroundColor = Color.mainButtonDisabledColor
            loginButton.isEnabled = false
            return false
        }
    }
    
    
    var phoneNumText: String? {
        return inputView1.textField.text
    }
    var codeText: String? {
        return inputView2.textField.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInputZone()
    }
}

private extension QuickLoginViewController {
    
    func setupInputZone() {
        inputView1.inputType = InputType.phoneNum
        inputView1.removeAllButtons()
        phoneView.addSubview(inputView1)
        
        inputView2.inputType = InputType.code
        inputView2.secureButton.removeFromSuperview()
        //点击发送验证码
        inputView2.codeButton.codeAction = { [weak self] button in
            return self?.phoneNumText
        }
        codeView.addSubview(inputView2)
    }
    
}












