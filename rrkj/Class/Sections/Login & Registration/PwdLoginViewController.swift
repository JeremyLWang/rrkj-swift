//
//  PwdLoginViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/18.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class PwdLoginViewController: BaseViewController {
    @IBOutlet weak var loginButton: Button!
    @IBOutlet weak var numView: UIView!
    @IBOutlet weak var pwdView: UIView!
    private lazy var inputView1 = InputView.input
    private lazy var inputView2 = InputView.input
    var phoneNumText: String? {
        return inputView1.textField.text
    }
    var pwdText: String? {
        return inputView2.textField.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInputZone()
    }
    
    @IBAction func forgetPwdAction(_ sender: UIButton) {
        self.navigationController?.pushViewController(FindLoginPwdViewController(), animated: true)
    }
    
    @IBAction func registAction(_ sender: UIButton) {
         self.navigationController?.pushViewController(RegistViewController(), animated: true)
    }
    
}

private extension PwdLoginViewController {
    
    func setupInputZone() {
        
        inputView1.inputType = InputType.phoneNum
        inputView1.removeAllButtons()
        numView.addSubview(inputView1)
        
        
        inputView2.inputType = InputType.loginPwd
        inputView2.codeButton.removeFromSuperview()
        pwdView.addSubview(inputView2)
    }
}
