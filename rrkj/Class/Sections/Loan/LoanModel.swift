//
//  LoanModel.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/10.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import Foundation
import ObjectMapper

struct LoanModel: Mappable {
    var amountButton: String?
    var amounts: [Int]?
    var amountsMax: String?
    var amountsMin: String?
    var isContact: Int?
    var isShowInstallment: Int?
    var item: [Banner]?
    var messageURL: String?
    var messageNo: Int?
    var periodNum: [PeriodNum]?
    var interestRate: Double?
    var interestRateDes: String?
    var title: String?
    var titleV2: String?
    var unusedAmount: Int?
    var userLoanLogList: [String]?
    
    init?(map: Map) { }

    mutating func mapping(map: Map) {
        amountButton      <- map["amount_button"]
        amounts           <- map["amounts"]
        amountsMax        <- map["amounts_max"]
        amountsMin        <- map["amounts_min"]
        isContact         <- map["is_contact"]
        isShowInstallment <- map["is_show_installment"]
        item              <- map["item"]
        messageURL        <- map["message.message_url"]
        messageNo         <- map["message.message_no"]
        periodNum         <- map["period_num"]
        interestRate      <- map["service_fee.interest_rate"]
        interestRateDes   <- map["service_fee.interest_rate_des"]
        title             <- map["title"]
        titleV2           <- map["title_v2"]
        unusedAmount      <- map["unused_amount"]
        userLoanLogList   <- map["user_loan_log_list"]
    }
}

struct PeriodNum: Mappable {
    var pv: String?
    var pk: Int?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        pv <- map["pv"]
        pk <- map["pk"]
    }
}


struct Banner: Mappable {
    var img_url: String?
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        img_url <- map["img_url"]
    }
}
