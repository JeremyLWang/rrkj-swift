//
//  LoanViewController.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Kingfisher
import Dispatch
import ESPullToRefresh

class LoanViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var banner: UIView!
    @IBOutlet weak var textVerticalScrollView: UIView!
    @IBOutlet weak var topOffset: NSLayoutConstraint!
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    @IBOutlet weak var loanAmountLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var minimumLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var interestLabel: UILabel!
    @IBOutlet weak var timeLimitLabel: UILabel!
    @IBOutlet weak var applyButton: Button!
    @IBOutlet weak var containerBottom: NSLayoutConstraint!
    
    var monthAmount: Int?
    var loanModel: LoanModel?
    
    /*  设置为系统的pageControl样式利用dotType */
    private lazy var bannerScrollView: LTAutoScrollView = {
        let bannerScrollView = LTAutoScrollView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: banner.bounds.height))
        bannerScrollView.glt_timeInterval = 2.5
        bannerScrollView.didSelectItemHandle = {
            DLog("autoScrollView8 点击了第 \($0) 个索引")
        }
        bannerScrollView.images = ["home_banner_normal"]
        bannerScrollView.imageHandle = {(imageView, imageName) in
            imageView.image = UIImage(named: imageName)
        }
        let layout = LTDotLayout(dotColor: UIColor.white, dotSelectColor: Color.main, dotType: .default)
        /*设置dot的间距*/
        layout.dotMargin = 10
        /* 如果需要改变dot的大小，设置dotWidth的宽度即可 */
        layout.dotWidth = 8
        /*如需和系统一致，dot放大效果需手动关闭 */
        layout.isScale = false
        bannerScrollView.dotLayout = layout
        return bannerScrollView
    }()
    
    /*  文字上下轮播一行 */
    private lazy var textScrollView: LTAutoScrollView = {
        let textScrollView = LTAutoScrollView(frame: textVerticalScrollView.bounds)
        textScrollView.glt_timeInterval = 2.0
        textScrollView.scrollDirection = .vertical
        textScrollView.isDisableScrollGesture = true
        textScrollView.autoViewHandle = {[unowned self] in
            return self.customTextScrollSubviews(labelText: ["-------"])
        }
        return textScrollView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        topOffset.constant = CGFloat(NAV_HEIGHT);
        bottomOffset.constant = CGFloat(TABBAR_HEIGHT);
        containerBottom.constant = CGFloat(isIphoneX() ? 90 : isIphone8P() ? 70 : 0)
        fetchData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMainView()
        //极速申请
        self.applyButton.onClick = { [weak self] button in
            self?.loanApply(button: button)
        }
    }
    
}

//MARK: private methods
private extension LoanViewController {
    //构建主视图
    func setupMainView() {
        self.navigationItem.title = "人人快借";
        banner.addSubview(bannerScrollView)
        self.automaticallyAdjustsScrollViewInsets = false
        textVerticalScrollView.addSubview(textScrollView)
        slider.setThumbImage(#imageLiteral(resourceName: "home_slider_circle"), for: .normal)
        
        //构建下拉刷新
        let header: ESRefreshProtocol & ESRefreshAnimatorProtocol
        header = ESRefreshHeaderAnimator.init(frame: CGRect.zero)
        scrollView.es.addPullToRefresh(animator: header) {[weak self] in
            self?.fetchData()
        }
        scrollView.refreshIdentifier = String.init(describing: 1)
        scrollView.expiredTimeInterval = 10.0
    }
    
    func customTextScrollSubviews(labelText: [String]) -> [UIView] {
        var views = [UIView]()
        for index in 0 ..< labelText.count {
            let bottomView = UIView(frame: CGRect(x: 0, y: 0, width: textVerticalScrollView.bounds.width, height: textVerticalScrollView.bounds.height))
            bottomView.backgroundColor = UIColor.white
            views.append(bottomView)
            let label1 = UILabel(frame: CGRect(x: 0, y: 0, width: textVerticalScrollView.bounds.width, height: textVerticalScrollView.bounds.height))
            label1.textColor = Color.main
            label1.text = labelText[index]
            label1.font = UIFont.systemFont(ofSize: 13)
            bottomView.addSubview(label1)
        }
        return views
    }
    
    func fetchData() {
        HTTPManager.session.getRequest(forKey: kCreditAppAppIndex, succeed: { (result, code, unwrapNormal) in
            if code == 0 && unwrapNormal {
                //DLog("loan ====>\(result)")
                let loan = LoanModel(JSON: result);
                self.loanModel = loan
                guard let loanModel = loan else { return };
                DispatchQueue.main.async {
                    self.scrollView.es.stopPullToRefresh()
                    self.refreshUI(loanModel: loanModel)
                }
            }
        }) { (msg) in
            DLog(msg)
        }
    }
    //刷新UI
    func refreshUI(loanModel: LoanModel) {
        //banner
        var images: [String] = []
        if let item = loanModel.item {
            item.forEach { (banner) in
                images.append(banner.img_url!)
            }
        }
        bannerScrollView.images = images
        bannerScrollView.imageHandle = {(imageView, imageName) in
            imageView.kf.setImage(with: URL(string: imageName))
        }
        //通知
        if let userLoanLogList = loanModel.userLoanLogList {
            //解决引用循环的问题
            textScrollView.autoViewHandle = {[unowned self] in
                return self.customTextScrollSubviews(labelText: userLoanLogList)
            }
        }
        
        slider.minimumValue = 0.0;
        slider.maximumValue = Float((loanModel.amounts?.count)!-1)
        slider.value = slider.maximumValue
        
        if let amountsMax = loanModel.amountsMax {
            maxLabel.text = amountsMax
            loanAmountLabel.text = maxLabel.text;
        }
        
        if let amountsMin = loanModel.amountsMin {
            minimumLabel.text = amountsMin
        }
        
        //月利率
        interestLabel.text = String(format: "%.2lf%%", loanModel.interestRate! * 100)
        //借款期限
        timeLimitLabel.text = loanModel.periodNum?.first?.pv
        monthAmount = loanModel.periodNum?.first?.pk
    }
}

private extension LoanViewController {
    
    func loanApply(button: UIButton) {
        navigationController?.pushViewController(LoanApplyViewController(), animated: true);
    }
    
    //slider滑动
    @IBAction func sliderMove(_ sender: UISlider) {
        var money = Int(round(sender.value))
        if let loanModel = loanModel, !(loanModel.amounts?.isEmpty)!{
            money = loanModel.amounts![money] / 100
        } else {
            money += 1
            money *= 100
        }
        loanAmountLabel.text = String(format: "%.2lf", Double(money))
    }
    //点击月利率
    @IBAction func interestClick(_ sender: UIButton) {
//        Alert.showMsgAlert(forController: self,
//                                buttonTittle: "知道了",
//                                content: "月利率与投资人的资金成本相关，此处展示仅作为参考 ")
//        let msg = """
//<p>由于您已经开通了江西银行存管账户，根据您曾授权的《江西银行网络交易资金账户服务三方协议》，极速钱包将委托相关平台方，使用您的银行存管账户，以便完成借款、还款等相关操作。</p>
//<br>
//<p>请确认开通存管时的银行卡信息（光大银行，尾号8976）准确无误，以免影响借款的发放。</p>
//"""
//        Alert.showDescAlert(forController: self,
//                            msg: msg,
//                            title: "重要声明")
    }
    
    //点击借款期限
    @IBAction func timeSelect(_ sender: UIButton) {
        
    }
    
}











