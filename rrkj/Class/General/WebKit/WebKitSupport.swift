//
//  WebKitSupport.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import WebKit

class WebKitSupport: NSObject {
    //在类的外面不可以执行set操作
    private(set) lazy var processpool: WKProcessPool = {
        let processpool = WKProcessPool();
        return processpool;
    }()
    
    static let suport: WebKitSupport = {
        let support = WebKitSupport();
        return support;
    }();
    
    static func fetchLoginUserSession() ->String {
        var cookiStr = "";
        if UserManager.default.isLogin {
            cookiStr = "\(kSessionID)=\(String(describing: UserManager.default.sessionID))";
        }
        return cookiStr;
    }
    
    static func injectJSForWebview(webview: WKWebView) {
        UserManager.default.handleCookieForURLString(url: (webview.url?.absoluteString)!);
        let JSFuncString = """
    function setCookie(cname,cvalue,exdays)\
    {\
    var d = new Date();\
    d.setTime(d.getTime()+(exdays*24*60*60*1000));\
    var expires = 'expires='+d.toGMTString();\
    document.cookie = cname + '=' + cvalue + '; ' + expires + ';path=/';\
    }
    """;
        var JSCookieString = JSFuncString;
        if let userSessionID = UserManager.default.sessionID {
            let excuteJSString = "setCookie('SESSIONID', '\(userSessionID)', 12);";
            JSCookieString += excuteJSString;
        }
        
        if !JSFuncString.isEmpty && JSFuncString.count >= JSCookieString.count{
            //执行JS
            webview.evaluateJavaScript(JSCookieString, completionHandler: nil);
        }
    }
}
