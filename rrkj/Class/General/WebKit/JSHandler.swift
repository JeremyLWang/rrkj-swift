//
//  JSHandler.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import WebKit
import Toast_Swift

struct WebPastEntity {
    var text: String = "";
    var tip: String = "";
}

class JSHandler: NSObject {
    weak private(set) var webVC: SuperWebController?
    private(set) var configuration: WKWebViewConfiguration
    var redAttach: AttachView?
    //交互处理的事件
    let handlerEvents: [String] = {
        return ["backPage",
                "Share",
                "returnNativeMethod",
                "copyTextMethod",
                "makePhoneCall",
                "checkAppIsInstalled",
                "hidenTabBar",
                "fetchingAlipay",
                "uploadPage",
                "toNativeWebview",
                "openBrowser"]
    }();
    
    
    var  entity: ShareEntity?;
    var  btnRight: UIButton?;
    var  orderId: String?;
    /**
     支付宝数据抓取进度
     */
    var progress: String?;
    var stopBtn: UIButton?;
    var topView: UIView?;
    var titleLabel: UILabel?;
    var uploadSheet: UIActionSheet?;
    /**分享成功回调*/
    var platformStr: String?;
    //分享成功后，分享的平台；QQ、微信
    var platformTypeStr: String?;
    
    init(webVC: SuperWebController, configuration: WKWebViewConfiguration) {
        self.webVC = webVC;
        self.configuration = configuration;
        super.init()
    }
    
    public func cancelHandler() {
        
    }
}

//MARK: private methods
extension JSHandler {
    private func registJSEvent() {
        handlerEvents.forEach { (eventName) in
            configuration.userContentController.add(self, name: eventName);
        }
    }
}

extension JSHandler: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        //message.body  --  Allowed types are NSNumber, NSString, NSDate, NSArray,NSDictionary, and NSNull.
        switch message.name {
        case "backPage":
            if ((webVC?.presentationController) != nil) {
                webVC?.dismiss(animated: true, completion: nil);
            } else {
                webVC?.navigationController?.popViewController(animated: true);
            }
        case "Share":
            shareMethod(paramStr: message.body as? String)
        case "returnNativeMethod":
            returnNativeMethod(paramStr: message.body as? String)
        case "copyTextMethod":
            copyTextMethod(paramStr: message.body as? String)
        case "makePhoneCall":
            makePhoneCall(paramStr: message.body as? String)
        case "checkAppIsInstalled":
            checkAppIsInstalled(paramStr: message.body as? String)
        case "fetchingAlipay":
            goneLayout(paramStr: message.body as? String)
        case "uploadPage":
            uploadPage()
        case "hidenTabBar":
            hiddenTabBar()
        case "toNativeWebview":
            toNativeWebview(paramStr: message.body as? String)
        case "openBrowser":
            openBrowser(paramStr: message.body as? String)
        default:
            UIApplication.shared.keyWindow?.makeToast("no native method to call")
        }
    }
    
    
}

//MARK: JS调用原生方法
extension JSHandler {
    private func shareMethod(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
        var jsonString = paramStr
        
        jsonString = jsonString.clearString(jsonString: jsonString)
        var jsonData = jsonString.data(using: String.Encoding.utf8)
        
        //TO DO ........
        if let jsonData = jsonData {
            
            do {
                var param = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
            } catch {
                
            }
            
            
        }
    }
    
    private func returnNativeMethod(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
    }
    
    private func copyTextMethod(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
    }
    
    private func makePhoneCall(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
    }
    
    private func checkAppIsInstalled(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
    }
    
    private func goneLayout(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
    }
    
    private func uploadPage() {
        
    }
    
    private func hiddenTabBar() {
        
    }
    
    private func toNativeWebview(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
    }
    
    private func openBrowser(paramStr: String?) {
        guard let paramStr = paramStr else {
            return
        }
    }
}





















