//
//  CustomTabbar.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class CustomTabbar: UIView {
    var selectedButtonIndex: Int = 0;
    var clickTab: ((Int)->Void)?
    
    private var previousBtn = UIButton();
    
    public func setupTabbar(withTitle title: String,
                            normalImage: String,
                            selectedImage: String,
                            index: Int) {
        let barBtn = UIButton(type: .custom)
        barBtn.tag = 500 + index;
        
        let buttonW = self.frame.size.width/4;
        let buttonH = self.frame.size.height;
        //强制转换类型CGFloat(index)
        barBtn.frame = CGRect(x: buttonW * CGFloat(index),
                              y: 0,
                              width: buttonW,
                              height: buttonH);
        //状态图片
        barBtn.setImage(UIImage(named:normalImage), for: .normal)
        barBtn.setImage(UIImage(named:selectedImage), for: .selected);
        
        barBtn.setTitle(title, for: .normal);
        barBtn.titleLabel?.font = UIFont.systemFont(ofSize: 10);
        barBtn.titleLabel?.textAlignment = .center;
        
        barBtn.imageView?.contentMode = .center;
        //UIViewContentModeScaleAspectFit;
        barBtn.titleLabel?.contentMode = .scaleAspectFit;
        
        //字体颜色
        barBtn.setTitleColor(UIColor(hex: "#BABFC9"), for: .normal);
        barBtn.setTitleColor(Color.main, for: .selected);
        //设置文字偏移：向下偏移图片高度＋向左偏移图片宽度 （偏移量是根据［图片］大小来的，这点是关键）
        barBtn.titleEdgeInsets = UIEdgeInsets(top: barBtn.imageView!.frame.size.height,
                                              left: -barBtn.imageView!.frame.size.width,
                                              bottom: 0,
                                              right: 0);
        //设置图片偏移：向上偏移文字高度＋向右偏移文字宽度 （偏移量是根据［文字］大小来的，这点是关键）
        barBtn.imageEdgeInsets = UIEdgeInsets(top: -barBtn.titleLabel!.bounds.size.height,
                                              left: 0,
                                              bottom: 0,
                                              right: -barBtn.titleLabel!.bounds.size.width);
        barBtn.addTarget(self, action: #selector(changeViewController(button:)), for: .touchUpInside);
        self.addSubview(barBtn);
    }
    
    //点击按钮
    @objc private func changeViewController(button: UIButton) {
        let btnTag = button.tag - 500;
        
        if self.selectedButtonIndex != btnTag {
            self.selectedButtonIndex = btnTag;
            clickTab?(self.selectedButtonIndex)
            //使上一个变为非选中
            previousBtn.isSelected = !previousBtn.isSelected;
            previousBtn = button;
            previousBtn.isSelected = true;//当前选择
        }else {
            clickTab?(0)
            previousBtn.isSelected = true;
        }
    }
    
    public func setFirstButtonSelect() {
        //默认选择
        let firstSelect = self.viewWithTag(500) as! UIButton;
        previousBtn = firstSelect;
        self.changeViewController(button: firstSelect);
    }
}
