//
//  TabBarController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    private lazy var customTabBar: CustomTabbar = {
        let customTabBar = CustomTabbar(frame: self.tabBar.bounds);
        customTabBar.isUserInteractionEnabled = true;
        customTabBar.backgroundColor = UIColor.white;
        customTabBar.clickTab = {(idx) in
            self.selectedIndex = idx
        }
        tabBar.addSubview(customTabBar)
        return customTabBar;
    } ();
    
    //单例创建
    static let `default`: TabBarController = {
        let makeTabVC = TabBarController();
        return makeTabVC;
    }();
    
    
    /**
     * 自定义的tabbar在iOS8 中重叠的情况.就是原本已经移除的UITabBarButton再次出现.
     在iOS8 是允许动态添加tabbaritem的
     */
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        for child in tabBar.subviews as [UIView] {
            if child.isKind(of: NSClassFromString("UITabBarButton")!) {
                child.removeFromSuperview();
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        for obj in self.tabBar.subviews as[UIView] {
            if obj != customTabBar {
                obj.removeFromSuperview();
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.tabBar.barTintColor = UIColor.white;
        self.tabBar.backgroundColor = UIColor.white;
        view.backgroundColor = UIColor.white;
        
        setupControllersAndTabBars();
    }
}

extension TabBarController {
    private func setupControllersAndTabBars() {
        let titles = ["借款", "还款", "商城", "我的"];
        let normalImages = ["tabicon_01", "tabicon_02", "tab_recommend", "tabicon_04"];
        let selectedImages = ["tabicon_01_pressed", "tabicon_02_pressed", "tab_recommend_selected", "tabicon_04_pressed"];
        let controllers = ["LoanViewController", "RepaymentViewController", "MallViewController", "MineViewController"];
        
        for idx in controllers.indices {
            let str = controllers[idx];
            let nav = addChildViewController(childControllerName: str);
            if nav != nil {
                self.addChildViewController(nav!);
            }
            customTabBar.setupTabbar(withTitle: titles[idx],
                                     normalImage: normalImages[idx],
                                     selectedImage: selectedImages[idx],
                                     index: idx);
        }
        customTabBar.setFirstButtonSelect();
    }
    
    //类名反射
    private func addChildViewController(childControllerName: String) -> NavigationController? {
        
        // 1.获取命名空间
        guard let classString = Bundle.main.infoDictionary!["CFBundleExecutable"] else {
            DLog("命名空间不存在");
            return nil;
        }
        // 2.通过命名空间和类名转换成类
        let cls : AnyClass? = NSClassFromString((classString as! String) + "." + childControllerName);
        
        // swift 中通过Class创建一个对象,必须告诉系统Class的类型
        //guard只有在条件不满足的时候才会执行这段代码
        guard let classType = cls as? BaseViewController.Type else {
            DLog("无法转换成BaseViewController");
            return nil;
        }
        
        // 3.通过Class创建对象
        let childController = classType.init();
        
        return NavigationController(rootViewController: childController);
    }
}


extension TabBarController {
    func popTo(viewController controller: UIViewController) {
        guard let controllers = self.viewControllers else { return }
        let nav = controllers[self.selectedIndex] as? NavigationController
        nav?.popToViewController(controller, animated: true)
    }
}
