//
//  BaseViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var showNavigationBar: Bool = true
    var showNextStep: Bool = false      //是否走认证向导
    var isStatusLight: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        UIApplication.shared.statusBarStyle = isStatusLight ? .lightContent : .default;
        
         self.navigationController?.navigationBar.isHidden = !showNavigationBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Color.backgroundColor;
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

extension BaseViewController {    
    /// 去除NavigationBar的线
    func hideNavigationBarLine() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    /// 登录页面
    func signIn() {
        self.present(LoginViewController.default, animated: true, completion: nil);
    }
    
    
}
