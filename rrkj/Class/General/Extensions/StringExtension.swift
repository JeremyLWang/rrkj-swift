//
//  StringExtension.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/10.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import Foundation

extension String {
    func clearString(jsonString: String) -> String {
        var str = jsonString
        let specialCharacters: [String] = ["\n","\t"]
        specialCharacters.forEach { (character) in
            str = jsonString.replacingOccurrences(of: character, with: "")
        }
        return str
    }
    
    //把传入的参数按照get的方式打包到url后面。
   static func addQueryStringTo(URL url: String, param: Dictionary<String, String>?) -> String? {
        guard let dict = param else { return nil}
        var urlWithQueryString = url
        if dict.count > 0 {
            for (key,value) in dict {
                if urlWithQueryString.contains("?") {
                    urlWithQueryString.append("&\(String.urlEscape(unencodedString: key))=\(String.urlEscape(unencodedString: value))")
                } else {
                    urlWithQueryString.append("?\(String.urlEscape(unencodedString: key))=\(String.urlEscape(unencodedString: value))")
                }
            }
        }
        return urlWithQueryString
    }
    
    static func urlEscape(unencodedString: String) ->String {
        return CFURLCreateStringByAddingPercentEscapes(nil,
                                                       unencodedString as CFString,
                                                       nil,
                                                       "!*'\"();:@&=+$,/?%#[]% " as CFString,
                                                       CFStringBuiltInEncodings.UTF8.rawValue
            ) as String;
    }
    
    var unicodeStr: String {
        let tempStr1 = self.replacingOccurrences(of: "\\u", with: "\\U")
        let tempStr2 = tempStr1.replacingOccurrences(of: "\"", with: "\\\"")
        let tempStr3 = "\"".appending(tempStr2).appending("\"")
        let tempData = tempStr3.data(using: .utf8)
        var returnStr:String = ""
        do {
            returnStr = try PropertyListSerialization.propertyList(from: tempData!, options: PropertyListSerialization.MutabilityOptions.mutableContainers, format: nil) as! String
        } catch {
            DLog(error)
        }
        return returnStr.replacingOccurrences(of: "\\r\\n", with: "\n")
    }
    
}
