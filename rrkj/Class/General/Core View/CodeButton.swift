//
//  CodeButton.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/21.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Dispatch

class CodeButton: UIButton {

    static var code: CodeButton {
        get {
            return Bundle.main.loadNibNamed("CodeButton", owner: self, options: nil)?.last as! CodeButton
        }
    }
    
    var codeAction: ((UIButton)->String?)?
    
    override func draw(_ rect: CGRect) {
        self.addTarget(self, action:#selector(click(item:)), for:.touchDown)
    }
    
    @objc private func click(item: UIButton){
        item.isSelected = !item.isSelected
        let num = self.codeAction?(item)
        guard let phone = num, !phone.isEmpty else {
            toastShow(text: "请输入合法的手机号码")
            return
        }
        CodeButton.timerCountDown(forButton: item, title: "秒") { }
        CodeSecure.md5Encrypt(phoneNum: phone, encryptedCallBack: { (sign, random) in
            let dict: Dictionary<String, Any> = ["phone": phone,
                                                 "sign": sign,
                                                 "random": random]
            CodeSecure.codeSend(param: dict)
        })
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupButton()
    }
    
    private func setupButton() {
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        self.layer.borderColor = Color.main.cgColor
        self.layer.borderWidth = 1;
        self.isEnabled = true;
        self.setTitle("获取", for: .normal)
        self.setTitleColor(Color.main, for: .normal)
    }
    
    static func timerCountDown(forButton btn: UIButton,
                               title: String = "",
                               finish: @escaping()->Void) {
        if !btn.isEnabled { return }
        btn.setTitleColor(Color.color_DC_C6, for: .normal)
        btn.layer.borderColor = Color.color_CC_C8.cgColor
        
        var timeout: Int = 60
        let timer = DispatchSource.makeTimerSource() as! DispatchSource
        //倒计时时间
        timer.schedule(wallDeadline: .now(), repeating: 1.0)
        timer.setEventHandler {
            //每秒执行
            if timeout <= 0 {
                //倒计时结束，关闭
                timer.cancel()
                DispatchQueue.main.async(execute: {() -> Void in
                    btn.isEnabled = true
                    btn.layer.borderColor = Color.main.cgColor
                    btn.setTitle("获取", for: .normal)
                    btn.setTitleColor(Color.main, for: .normal)
                    //设置界面的按钮显示 根据自己需求设置
                    finish()
                })
            } else {
                let seconds: Int = timeout % 61
                let secondString = String(format: "%.2d", seconds)
                DispatchQueue.main.async(execute: {() -> Void in
                    btn.isEnabled = false
                    //设置动画
                    UIView.beginAnimations(nil, context: nil)
                    UIView.setAnimationDuration(1)
                    let btnTitle = title.isEmpty ? "\(secondString)" : "\(secondString)\(title)"
                    btn.setTitle(btnTitle, for: .normal)
                    UIView.commitAnimations()
                })
                timeout -= 1
            }
        }
        timer.resume()
    }
}



