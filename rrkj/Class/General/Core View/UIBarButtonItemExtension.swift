//
//  UIBarButtonItemExtension.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    /*
     https://stackoverflow.com/questions/25156377/what-is-the-difference-between-static-func-and-class-func-in-swift
     */
    class func creatBarItem(image: String, highImage: String, title: String, tagget: Any, action: Selector) -> UIBarButtonItem {
        let button = UIButton.init(type: UIButtonType.custom)
        button.setTitle(title, for: UIControlState.normal)
        button.setImage(UIImage.init(named: image), for: UIControlState.normal)
        button.setImage(UIImage.init(named: highImage), for: UIControlState.highlighted)
        button.addTarget(tagget, action: action, for: UIControlEvents.touchUpInside)
        button.sizeToFit()
        return UIBarButtonItem.init(customView: button)
    }
    
}
