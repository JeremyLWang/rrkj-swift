//
//  TextField.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/21.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

enum InputType {
    case phoneNum   //数字
    case code       //数字
    case loginPwd   //字母与数字与符号
    case name       //字母
    case IDNum      //字母与数字
}

class InputView: UIView {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var line: UIView!
    private var text: String = ""
    
    private var isButtonsRemoved: Bool? {
        didSet {
            if let removed = isButtonsRemoved {
                self.textField.clearButtonMode = removed ? .whileEditing : .never;
            }
        }
    }
    
    var inputType: InputType? {
        didSet {
            guard let type = inputType else {
                return
            }
            switch type {
            case .phoneNum:
                self.textField.placeholder = "请输入手机号码"
                self.textField.keyboardType = .phonePad
            case .code:
                self.textField.placeholder = "请输入验证码"
                self.textField.keyboardType = .phonePad
            case .IDNum:
                self.textField.placeholder = "请输入身份证号"
                self.textField.keyboardType = .default
            case .loginPwd:
                self.textField.placeholder = "请输入密码"
                self.textField.keyboardType = .default
                self.textField.isSecureTextEntry = true //密码默认安全模式
            case .name:
                self.textField.placeholder = "请输入真实姓名"
                self.textField.keyboardType = .default
            }
        }
    }
    
    @IBOutlet weak var codeButton: CodeButton!
    
    @IBOutlet weak var secureButton: UIButton!
    
    @IBAction func switchSecureMode(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textField.isSecureTextEntry = !textField.isSecureTextEntry
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.delegate = self;
    }
    
    static var input: InputView {
        get {
            return Bundle.main.loadNibNamed("InputView", owner: self, options: nil)?.last as! InputView;
        }
    }
    
    var placeHolder: String? {
        get {
            return self.textField.placeholder
        }
        set {
            self.textField.placeholder = newValue
        }
    }
}

extension InputView {
    func removeAllButtons() {
        self.codeButton.removeFromSuperview()
        self.secureButton.removeFromSuperview()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.subviews.forEach { [weak self] in
            self?.isButtonsRemoved = ($0 != self?.codeButton && $0 != self?.secureButton)
        }
    }
}

extension InputView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.line.backgroundColor = Color.main
        return true;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.line.backgroundColor = Color.color_DC_C6
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DLog(string)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        return true
    }
}


