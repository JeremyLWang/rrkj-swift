//
//  CodeSecure.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/24.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import Foundation
import CryptoSwift

private let publicKey = "jsqb#666*5"

class CodeSecure: NSObject {
    static func md5Encrypt(phoneNum num: String?,
                           encryptedCallBack: @escaping(String, String)->Void) {
        guard let num = num else { return }
        HTTPManager.session.getRequest(forKey: kCreditAppRandom, succeed: { (json, code, unwrapNormal) in
            if code == 0 && unwrapNormal{
                if let random = json["random"] as? String {
                    let sign = "\(num)\(random)\(publicKey)"
                    let md5Sigh = sign.md5()
                    encryptedCallBack(md5Sigh, random)
                }
            }
        }) { (errMsg) in
            
        }
    }
    
    static func codeSend(param: Dictionary<String, Any>?) {
        HTTPManager.session.postRequest(forKey: kUserQuickLoginCode, param: param, succeed: { (json, code, unwrapNormal) in
            if code == 0 && unwrapNormal{
                
            } else {
                
            }
        }) { (errMsg) in
            
        }
    }
    
}
