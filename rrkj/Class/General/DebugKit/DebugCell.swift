//
//  DebugCell.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/8/16.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class DebugCell: UITableViewCell {

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var devLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        devLabel.textColor = Color.main
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
