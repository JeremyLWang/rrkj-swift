//
//  DebugViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/8/16.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Toast_Swift

class DebugViewController: BaseViewController {
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var sw: UISwitch!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var container: UIView!
    var chooseComplete: (()->Void)?
    private var type: ConfigInfoType = .dev
    private var userData = [[String: Any]]()
    private var lastTimeInsertData: Dictionary<String, String>? {
        get {
            return UserDefaults.standard.object(forKey: "lastTime") as? Dictionary<String, String>
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "lastTime")
        }
    }
    private var envis:[[String: String]] {
        get {
            return [
                ["branch_name":"rrkj-app-25-ios.dev.cs",  //分支名称
                "branch_desc":"人人快花"] //rrkj-app-4-app-index.dev.cs
            ]
        }
    }
    @IBOutlet weak var `switch`: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.rowHeight = 50.0
        self.tableView.register(UINib(nibName: "DebugCell", bundle: nil), forCellReuseIdentifier: "debugCell")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

//MARK: UITableViewDelegate, UITableViewDataSource
extension DebugViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.isEmpty ? envis.count : userData.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: WIDTH_OF_SCREEN, height: 30))
        label.text = "  可选测试分支(点击即可进入所选环境)";
        label.font = UIFont.systemFont(ofSize: 14)
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        return label
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
        enviChoose(envi: !userData.isEmpty ?
            userData[indexPath.row]["branch_name"]! as! String :
            envis[indexPath.row]["branch_name"]!  )
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "debugCell", for: indexPath) as! DebugCell
        var text = ""
        var detailText = ""
        if !userData.isEmpty  {
            if let descText = userData[indexPath.row]["branch_desc"], let nameText = userData[indexPath.row]["branch_name"] {
                text = nameText as! String
                detailText = descText as! String
            }
        } else {
            if let branchName = envis[indexPath.row]["branch_name"], let branchDesc = envis[indexPath.row]["branch_desc"] {
                text = branchName
                detailText = branchDesc
            } else {
                UIApplication.shared.keyWindow?.makeToast("请先设置分支名")
            }
        }
        cell.devLabel.text = text;
        cell.detailLabel.text = detailText;
        cell.backgroundColor = UIColor.clear
        return cell;
    }
}

private extension DebugViewController {
    @IBAction func switchStatus(_ sender: UISwitch) {
        //用来切换域名不同的情况
        if sender.isOn {
            TearUpBox.default.swipeAction()
            TearUpBox.default.dimissHandler = {
               self.switch.isOn = false
            }
        }
    }
    
    @IBAction func clickACtion(_ sender: UIButton) {
        //选择正式环境
        let config = ConfigManager.default
        config.configInfoType = .production
        type = config.configInfoType
        config.getConfigSuccess = {
            self.chooseComplete?()
        }
        config.getConfigWithURL(url: kReleaseURL)
    }
    
    func setupViews() {
        sw.onTintColor = Color.main
        sw.tintColor = Color.main
        
        backGroundView.backgroundColor = Color.main.withAlphaComponent(0.3)
        
        versionLabel.text = "version:\(DeviceInfo.appVersion())"
        
        let holderText = "输入您需要的分支 or 选择下面的分支";
        let placeHolder = NSMutableAttributedString(string: holderText)
        placeHolder.addAttribute(.foregroundColor, value: Color.main, range: NSMakeRange(0, placeHolder.length))
        placeHolder.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 14), range: NSMakeRange(0, placeHolder.length))
        textfield.attributedPlaceholder = placeHolder
        
        let titles = ["输入完成","使用之前","手动添加"]
        for (idx, title) in titles.enumerated() {
            let containerW = WIDTH_OF_SCREEN - 30
            let buttonWidth = (containerW-40)/CGFloat(titles.count)
            let buttton = UIButton(frame: CGRect(x: 0 + (buttonWidth+20) * CGFloat(idx),
                                                 y: 0,
                                                 width: buttonWidth,
                                                 height: 40))
            buttton.backgroundColor = Color.main;
            buttton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            buttton.setTitleColor(UIColor.white, for: .normal)
            buttton.tag = 1000 + idx
            buttton.layer.cornerRadius = 20;
            buttton.layer.masksToBounds = true;
            buttton.setTitle(title, for: .normal)
            buttton.addTarget(self, action: #selector(inputConfirmAction(sender:)), for:.touchUpInside)
            container.addSubview(buttton)
        }
        
        if let insertData = lastTimeInsertData {
            userData.append(insertData)
            userData.append(contentsOf: envis)
        }
    }
    
    @objc func inputConfirmAction(sender: UIButton) {
        let environment = sender.tag - 1000
        switch environment {
        case 0:
            guard let text = textfield.text else {
                UIApplication.shared.keyWindow?.makeToast("您还没有输入任何环境分支，您可以选择使用之前，或者用下面列表中的分支")
                return
            }
            let config = ConfigManager.default
            config.configInfoType = .production
            type = config.configInfoType
            config.getConfigSuccess = {
                self.chooseComplete?()
            }
            config.getConfigWithURL(url: "http://\(text)\(kDebugDomainName)/credit/web/credit-app/config")
        case 1:
            let config = ConfigManager.default
            config.configInfoType = type
            config.getConfigWithURL(url: nil)
            config.getConfigSuccess = {
                self.chooseComplete?()
            }
        default:
            confirmInsertEnvi()
        }
    }
    //选择环境分支
    func enviChoose(envi: String) {
        let config = ConfigManager.default
        config.configInfoType = .dev
        type = config.configInfoType
        config.getConfigSuccess = {
            self.chooseComplete?()
        }
        config.getConfigWithURL(url: "http://\(envi)\(kDebugDomainName)/credit/web/credit-app/config")
    }
    //点击手动
    func confirmInsertEnvi() {
        self.view.endEditing(true)
        var msg = ""
        
        if lastTimeInsertData?["branch_desc"]! != "手动添加的分支" {
            msg = "您确定要添加这个环境分支吗？"
        } else  {
            msg = "只能手动添加一个分支环境，如果您添加本次则上次会被覆盖，您确定要继续吗？"
        }
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let confirm = UIAlertAction(title: "confirm", style: .default) { (action) in
            let dict = ["branch_name":self.textfield.text!,
                        "branch_desc":"手动添加的分支"]
            self.lastTimeInsertData = dict
            self.userData.removeAll()
            self.userData.append(dict)
            self.userData.append(contentsOf: self.envis)
            self.textfield.text = ""
            self.tableView.reloadData()
        }
        let cancle = UIAlertAction(title: "cancle", style: .default, handler: nil)
        alert.addAction(confirm)
        alert.addAction(cancle)
        
        self.present(alert, animated: true, completion: nil)
    }
}














