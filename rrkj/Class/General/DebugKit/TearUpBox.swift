//
//  SandBoxController.swift
//  PersitentStoreData
//
//  Created by Jeremy Wang on 2018/8/15.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

enum BoxDataType {
    case embedded
    case directory
    case file
}

struct BoxData {
    var name: String
    var path: String
    var dataType: BoxDataType
}

class SandBoxController: UITableViewController {

    var datas: Array<BoxData> = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "BoxCell")
        fetch(path: "")
    }
}
// MARK: - Table view data source & delegate
extension SandBoxController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BoxCell", for: indexPath)
        cell.textLabel?.text = datas[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let boxData = datas[indexPath.row]
        switch boxData.dataType {
        case .embedded:
            fetch(path: (boxData.path as NSString).deletingLastPathComponent)
        case .directory:
            fetch(path: boxData.path)
        case .file:
            shareFiles(byPath: boxData.path)
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0;
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: self.tableView.bounds.size.width-45, y: 5, width: 40, height: 40)
        btn.setTitle("X", for: .normal);
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 35)
        btn.setTitleColor(UIColor.white, for: .normal);
        btn.backgroundColor = UIColor.red
        btn.layer.cornerRadius = 20;
        btn.layer.masksToBounds = true;
        btn.addTarget(self, action: #selector(dimiss), for: .touchUpInside);
        view.addSubview(btn)
        return view
    }
    @objc func dimiss() {
        print("dismiss")
        self.view.window?.isHidden = true;
        TearUpBox.default.dimissHandler?()
    }
}


private extension SandBoxController {
    func fetch(path: String) {
        var files: Array<BoxData> = []
        let fm = FileManager.default
        var targetPath = path;
        
        if targetPath.isEmpty || targetPath == NSHomeDirectory() {
            targetPath = NSHomeDirectory()
        }
        else {
            let boxData = BoxData(name: "◀️", path: path, dataType: .embedded)
            files.append(boxData)
        }
        
        do {
            let paths = try fm.contentsOfDirectory(atPath: targetPath)
            paths.forEach { (path) in
                //not show hidden files
                if !(path as NSString).lastPathComponent.hasPrefix(".") {
                    let fullPath = (targetPath as NSString).appendingPathComponent(path)
                    var isDir: ObjCBool = ObjCBool(false);
                    fm.fileExists(atPath: fullPath, isDirectory: &isDir)
                    
                    if isDir.boolValue {
                        files.append(
                            BoxData(name: "📂 \(path)",
                            path: fullPath,
                            dataType: .directory));
                    } else {
                        files.append(
                            BoxData(name: "📃 \(path)",
                            path: fullPath,
                            dataType: .file));
                    }
                }
            }
            datas = files;
            tableView.reloadData()
        } catch let error as NSError {
            print("path get failure \(error), \(error.userInfo)")
            let alert = UIAlertController(title: "Warning⚠️⚠️⚠️", message: error.localizedDescription, preferredStyle: .alert)
            let cancle = UIAlertAction(title: "Understood😞", style: .cancel, handler: nil)
            alert.addAction(cancle)
            self.present(alert, animated: true, completion: nil);
        }
    }
    
    func shareFiles(byPath path: String) {
        let url = NSURL.fileURL(withPath: path)
        let controller = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        controller.excludedActivityTypes = [.addToReadingList]
        self.present(controller, animated: true, completion: nil)
    }
}

class TearUpBox {
    static let `default` = TearUpBox()
   public var dimissHandler: (()->Void)?
    lazy var aWindow: UIWindow = {
        let aWindow = UIWindow()
        let frame = CGRect(x: 30, y: 100, width: UIScreen.main.bounds.size.width-60, height: 400)
        aWindow.backgroundColor = UIColor.white
        aWindow.layer.borderColor = UIColor.lightGray.cgColor
        aWindow.layer.borderWidth = 2.0
        aWindow.windowLevel = UIWindowLevelStatusBar
        let sandVC = SandBoxController()
        aWindow.rootViewController = sandVC
        return aWindow
    }()
    
    func start() {
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        swipe.numberOfTouchesRequired = 1
        swipe.direction = .left
        UIApplication.shared.keyWindow?.addGestureRecognizer(swipe)
    }
    
    @objc func swipeAction() {
        if !self.aWindow.isHidden {
            self.aWindow.isHidden = true;
        } else {
            self.aWindow.isHidden = false;
        }
    }
}















