//
//  Alert.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/29.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import PopupDialog

class Alert: NSObject {
    
    static func showMsgAlert(forController controller: UIViewController,
                                  buttonTittle: String,
                                  content: String,
                                  animated: Bool = true) {
        // Create the dialog
        let popup = PopupDialog(title: "",
                                message: content,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                gestureDismissal: false,
                                hideStatusBar: false) { }
        let buttonOne = CancelButton(title: buttonTittle) { }
        popup.addButtons([buttonOne])
        controller.present(popup, animated: animated, completion: nil)
    }
    
    static func showAlert(forController controller: UIViewController,
                          buttonTittles: [String],
                          title: String = "",
                          content: String,
                          confirmAction: @escaping()->Void) {
        // Create the dialog
        let popup = PopupDialog(title: title,
                                message: content,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                gestureDismissal: false,
                                hideStatusBar: false) { }
        let cancle = CancelButton(title: buttonTittles.first ?? "") { }
        let confirm = DefaultButton(title: buttonTittles.last ?? "") {
            confirmAction()
        }
        
        popup.addButtons([cancle, confirm])
        controller.present(popup, animated: true, completion: nil)
    }
    
    
    /// 退出登录的alert
    static func showLogOut(forController controller: UIViewController) {
        Alert.showAlert(forController: controller,
                        buttonTittles: ["取消","确定"],
                        content: "您确定要退出登录吗？") {
                            DLog("exit now")
        }
    }
    
    static func showDescAlert(forController controller: UIViewController,
                              msg: String,
                              title: String) {
        // Create a custom view controller
        let descVC = DescViewController(nibName: "DescViewController", bundle: nil)
        // Create the dialog
        let popup = PopupDialog(viewController: descVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                gestureDismissal: false)
        if msg.contains("<") {
            let data = msg.data(using: .unicode)!
            if let attributedString = try? NSAttributedString(data: data,
                                                              options: [.documentType: NSAttributedString.DocumentType.html,],
                                                              documentAttributes: nil) {
                let anString = attributedString.mutableCopy() as? NSMutableAttributedString
                anString?.addAttributes([NSAttributedStringKey.foregroundColor : Color.color_66_C3], range: NSMakeRange(0, (anString?.length)!))
                descVC.content.attributedText = anString
            }
        } else {
            descVC.content.text = msg;
        }

        descVC.titleLabel.text = title;
        
        // Present dialog
        controller.present(popup, animated: true, completion: nil)
    }
    
}
