//
//  DescViewController.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/31.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit

class DescViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.font = UIFont.systemFont(ofSize: 17)
        titleLabel.textColor = Color.color_45_C2
        
        content.font = UIFont.systemFont(ofSize: 14)
        content.textColor = Color.color_66_C3
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

