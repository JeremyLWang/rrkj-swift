//
//  ConfigManager.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Dispatch
import Kingfisher

enum ConfigInfoType: Int {
    case production = 3   //正式环境
    case dev = 4          //测试环境
}

class ConfigManager: NSObject {
    var configInfoType: ConfigInfoType = .dev
    var getConfigSuccess: (()->Void)?
    private var configDic: Dictionary<String, Any>?
    private var urlDic: Dictionary<String, Any>?
    
    public static let `default` = ConfigManager()
    
    private func fullFillPath(path: String) ->String {
        return URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]).appendingPathComponent(path).absoluteString
    }
    
    //通过URL获取配置
    public func getConfigWithURL(url: String?) {
        /**
         下发配置文件每次只会拉取一次
         在拉下发配置文件的时候先用本地的配置文件初始化一下本地的变量
         */
        let fileName = "config_0\(configInfoType.rawValue).plist"
        let fileManager = FileManager.default
        
        ////有从网络上拉取的下发配置文件
        if fileManager.fileExists(atPath: fullFillPath(path: fileName)) {
            let dict =  NSDictionary(contentsOfFile: fullFillPath(path: fileName))
            if let dict = dict { //文件正常
                self.configDic = dict["item"] as? Dictionary<String, Any>
                self.urlDic = self.configDic?["dataUrl"] as? Dictionary<String, Any>
            } else { //文件异常（比如上次写失败了）取本地文件
                try? fileManager.removeItem(atPath: fullFillPath(path: fileName))
                let dic = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "config", ofType: "plist")!)
                if let dic = dic {
                    self.configDic = dic["item"] as? Dictionary<String, Any>
                    self.urlDic = self.configDic?["dataUrl"] as? Dictionary<String, Any>
                }
            }
        } else {//没有从网络上拉取缓存到本地的下发配置文件，则用本地默认的配置文件
            let localDict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "config", ofType: "plist")!)
            if let localDict = localDict {
                self.configDic = localDict["item"] as? Dictionary<String, Any>
                self.urlDic = self.configDic?["dataUrl"] as? Dictionary<String, Any>
            }
        }
        if let url = url {
            fetchConfigData(forURL: url, saveAs: fileName)
        }
    }
    
    public func getURLWithKey(key: String) ->String? {
        return urlDic![key] as? String
    }
    
    public func configForKey(key: String) -> Any {
        return configDic![key] ?? ""
    }
    
    public func getRandomRefreshDesc() -> String {
        var defaultString = "信用让生活更美好"
        if let configDic = configDic {
            guard let refreshText = configDic["refresh_text"] else { return defaultString }
            if refreshText is Array<String> {
                let texts = refreshText as! Array<String>
                if texts.count > 0 {
                    defaultString = texts[Int(arc4random()) % texts.count]
                }
            }
        }
        return defaultString
    }
}

//MARK: private methods
private extension ConfigManager {
    func fetchConfigData(forURL url: String, saveAs fileName: String) {
        HTTPManager.session.getRequest(forUrl: url, param: ["configVersion":DeviceInfo.appVersion()], succeed: { (json, code, unwrapNormal) in
            if code == 0 && unwrapNormal{
                let item = json["item"] as? Dictionary<String, Any>
                self.configDic = item
                guard let itemDict = item else { return }
                let urlDic = itemDict["dataUrl"] as? Dictionary<String, Any>
                self.urlDic = urlDic
                // 写缓存文件
                let data =  json as NSDictionary
                let result = data.write(toFile: self.fullFillPath(path: fileName), atomically: true)
                if result {
                    DLog("file write successful")
                }
                self.getConfigSuccess?()
                let imgPreLoad = self.configDic?["imgPreload"]//可能没有意义，因为imgPreload可能为空
                if let imgPreload = imgPreLoad, imgPreload is Array<String> {
                    let images = imgPreLoad as! Array<String>
                    if images.isEmpty { return }
                    images.forEach({ (url) in
                        // 预下载图片
                        let resource = ImageResource(downloadURL: URL(string: url)!, cacheKey: "text")
                        let retriveImageTask = KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil, completionHandler: {
                            (image, error, cacheType, imageURL) in
                            if error == nil {
                                DLog("获取图片成功")
                            } else {
                                DLog("获取图片失败")
                            }
                        })
                        retriveImageTask.cancel()
                    })
                }
            } else if code == -2 {
                self.getConfigSuccess?()
            }
        }) { (err) in
            DLog("")
        }
    }
}
