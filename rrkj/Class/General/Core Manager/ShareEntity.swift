//
//  ShareEntity.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import Foundation

struct ShareEntity {
    //0直接弹分享选择框 1右上角出来分享按钮 3只分享图片
    var type: String?;
    
    //0：默认分享方式  1：分享大图
    var share_data_type: String?;
    
    //分享title
    var share_title: String?;
    //分享描述
    var share_body: String?;
    //分享链接
    var share_url: String?;
    //分享图片
    var share_logo: String?;
    //单纯的只分享图片
    var share_image: Data?;
    
    //按钮文案
    var shareBtnTitle: String?;
    //是否分享
    var isShare: String?;
    //分享有奖描述
    var sharePageTitle: String?;
    
    //分享渠道['wx','wechatf','qq','qqzone','sina','sms'];
    var platform: String?;
    //分享调起方式
    var params: String?;
    //是否上报
    var shareIsUp: String?;
    //上报id
    var shareUpId: String?;
    //上报类型
    var shareUpType: String?;
    //上报url
    var shareUpUrl: String?;
    //js上报方法
    var callback: String?;
    
    var shareSuccess: (()->Void)?
}
