//
//  Cachekey.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/31.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import Foundation

//用户信息
let ZFBPath = "KDZFB";

//用户信息userLoginInfo
let userLoginInfo = "LoginUser";

/***************首页相关***************/

let kPublicNotice = "publicNotice";
let kHomeAnoucePublicNoticekey = "kHomeAnoucePublicNoticekey";
let kLoanDetailAnoucePublicNoticekey = "kLoanDetailAnoucePublicNoticekey";
let kPaymentDetailAnoucePublicNoticekey = "kPaymentDetailAnoucePublicNoticekey";
let kMineAnoucePublicNoticekey = "kMineAnoucePublicNoticekey";
let kWithdrawAnoucePublicNoticekey = "kWithdrawAnoucePublicNoticekey";

//首页未登录数据缓存
let unLoginhomeModel = "unLoginhomeModel";
//首页登录数据缓存
let loginHomeModel = "loginHomeModel";
//个人中心
let UserCenter = "UserCenter";
//首页弹框
let bounArray = "dataArray";

//通讯录
let AddressArray = "AddressArray";
let NewAddressArray = "NewAddressArray";
let CopyNewAddressArray = "CopyNewAddressArray";
//通讯录对应用户上传历史
let addressUserUpLoaded = "addressUserUpLoaded";

//红点
let loanRedPoint = "loanRedPoint";
let messageRedPoint = "messageRedPoint";
let couponRedPoint = "couponRedPoint";
let redPackRedPoint = "redPackRedPoint";
let verificationRedPoint = "verificationRedPoint";

//判断红点缓存
let loanPoint = "loanPoint";
let messagePoint = "messagePoint";
let couponPoint = "couponPoint";
let redPackPoint = "redPackPoint";
let verificationPoint = "verificationPoint";

//首次显示前程数据提示框
let QCAlertShow = "QCAlertShow";

//tabbar缓存
let tabbarRedPoint = "tabbarRedPoint";
let kTabbarPointCacheKey = "tabbarPoint";
let kMineRedPointCacheKey = "mineRedPoint";
let kMinePointCacheKey = "minePoint";
//支付宝加密数据
let zfbCapture = "zfbCapture";

//判断用户是否弹出前程提示框
let QCAlert = "QCAlert";

//tabBar的model数组
let kTabBarEntityArrayCacheKey = "tabBarEntityArray";

let tabBarImageUrlArray = "tabBarImageUrlArray";

let tabBarImagesIsCacheAll = "tabBarImagesIsCacheAll";

//首页通讯录是否全部上传成功
let contactIsUploadAll = "contactIsUploadAll";
