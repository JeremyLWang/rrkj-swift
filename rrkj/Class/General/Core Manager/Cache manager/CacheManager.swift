//
//  CacheManager.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/31.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Cache

enum CacheError: Error {
    case objectInvalid
    case keyIsInValid
}

class CacheManager: NSObject {
    static var `default` = CacheManager()
    
    ///directory path ==> file:///Users/jeremywang/Library/Developer/CoreSimulator/Devices/28BDBDA3-3043-4CA9-B781-2D78441973D6/data/Containers/Data/Application/FDBAD63B-B5B9-4A5D-A82B-D1262E116A18/Documents/MyPreferences
    let diskConfig = DiskConfig(
        name: projectShortName,
        expiry: .date(Date().addingTimeInterval(24*3600)),  //24Hours
        maxSize: 8*1024*1024*5,   //5MB
        directory: try! FileManager.default.url(for: .documentDirectory,
                                                in: .userDomainMask,
                                                appropriateFor: nil,
                                                create: true).appendingPathComponent("UserData"),
        protectionType: .complete
    )
    
    let memoryConfig = MemoryConfig(
        expiry: .never,
        countLimit: 20,   //20 obejcts
        /// 0 means no limit.
        totalCostLimit: 0
    )
    
     var storage: Storage {
        get {
            return try! Storage(diskConfig: diskConfig, memoryConfig: memoryConfig)
        }
    }
}

extension CacheManager: NSCacheDelegate {
    func cache(_ cache: NSCache<AnyObject, AnyObject>, willEvictObject obj: Any) {
        print("清除了---->\(obj)");
    }
}

extension CacheManager {
    /// 缓存数据
    ///
    /// - Parameters:
    ///   - object: 需要缓存的数据(需要遵循Codable)
    ///   - key: key
    /// - Returns: 是否成功
    /// - Throws: 抛出错误
    @discardableResult
    func cacheObject<T: Codable>(_ object: T?, forKey key: String?) throws ->Bool? {
        guard let k = key else { throw CacheError.keyIsInValid }
        guard let obj = object else { throw CacheError.objectInvalid }
        try? storage.setObject(obj, forKey: k)
        return try? storage.existsObject(ofType: T.self, forKey: k)
    }

    /// 取出数据
    ///
    /// - Parameter key: key
    /// - Returns: 返回缓存的数据
    /// - Throws: 抛出错误
    func getCacheObject<T: Codable>(forKey key: String?) throws ->T? {
        guard let k = key else { throw CacheError.keyIsInValid }
        return try? storage.object(ofType: T.self, forKey: k)
    }
    
    /// 根据对应的key删除数据
    ///
    /// - Parameter key: key
    /// - Throws: 抛出错误
    func removeCache(forKey key: String?) throws {
        guard let k = key, !k.isEmpty else { return }
        try? storage.removeObject(forKey: k)
    }
    
    /// 删除所有的缓存数据
    ///
    /// - Throws: 抛出错误
    func removeAllCache() throws {
        try? storage.removeAll()
    }
}
