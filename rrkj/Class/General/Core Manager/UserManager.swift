//
//  UserManager.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import ObjectMapper

struct UserModel: Mappable, Codable {
    var sessionid: String?
    var uid: Int?
    var username: String?            //手机号码
    var realname: String?             //真实姓名
    var real_pay_pwd_status: Int?    //交易密码状态
    var real_contact_status: Int?
    var face: Int?
    var special: String?
    var sex: String?                 //性别
    var regtime: String?             //注册时间
    
    init?(map: Map) { }

    mutating func mapping(map: Map) {
        sessionid           <- map["item.sessionid"]
        uid                 <- map["item.uid"]
        username            <- map["item.username"]
        realname            <- map["item.realname"]
        real_pay_pwd_status <- map["item.real_pay_pwd_status"]
        real_contact_status <- map["item.real_contact_status"]
        face                <- map["item.face"]
        special             <- map["item.special"]
        sex                 <- map["item.sex"]
        regtime             <- map["item.regtime"]
    }
}

class UserManager: NSObject {
    var sessionID: String?;                  //sessionID
    var uid: Int?;                           //用户ID
    var userName: String = "";               //用户名
    var realName: String = "";               //实名
    var real_pay_pwd_status: Int = 0;        //交易密码状态
    var sex: String = "";                    //性别
    var cookieHostArray: [String] = [];      //存cookie的host
    var loginSuccess: (()->Void)?            //登录成功回调
    private var userModel: UserModel?
    
    //登录成功
    var isLogin: Bool {
        get {
            return (uid != 0) && (!sessionID!.isEmpty) && (sessionID != nil);
        }
    }
    
    static var `default`: UserManager = {
        let sharedUser = UserManager();
        return sharedUser;
    }();
    
    private func initUserManager(userModel: UserModel) {
        uid = userModel.uid
        sessionID = userModel.sessionid;
        userName = userModel.username ?? "";
        realName = userModel.realname ?? "";
        real_pay_pwd_status = userModel.real_pay_pwd_status ?? 0;
        sex = userModel.sex ?? "";
    }
}

extension UserManager {
    
    //登录成功更新用户信息
    public func loginSuccessToUpdateUserInfo(userModel: UserModel) {
        
    }
    
    // 清除cookie的影响信息
    public func clearCookieInfluenceInfo() {
        
    }
    
    //处理cookie
    public func handleCookieForURLString(url: String) {
        
    }
    
    //退出登录
    public func logoutAndDeleteUserInfo() {
        
    }
    
    //更新实名认证信息
    public func updateRealName(name: String) {
        
    }
    
    //更新性别信息
    public func updateSex(sex: String) {
        
    }
    
    //更新交易密码信息
    public func updatePayPassWordtatus(status: Int) {
        
    }
    
    //清空登录信息(在code为-2的时候，清空信息)
    public func clearLoginStatusInfo() {
        
    }
    
    //通过拉取一个接口来获取登录态是否已经失效
    public func checkLoginStatus(status: ((_ sucOrFail: Bool) -> Void)) {
        
    }
    
    public func loginOut() {
        
    }
}
















