//
//  HTTPManager.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/14.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

enum HTTPVerbType {
    case Get
    case Post
}

class HTTPManager: NSObject {
    //单例创建
    static let session: HTTPManager = {
        let session = HTTPManager()
        return session
    }()
    
    
    private var appededUrl: Dictionary<String, String> {
        get {
            return ["appMarket":HttpParams.appMarket,
                    "appVersion":HttpParams.appVersion,
                    "clientType":HttpParams.clientType,
                    "deviceId":HttpParams.deviceId,
                    "deviceName":HttpParams.deviceName,
                    "osVersion":HttpParams.osVersion];
        }
    }
    
    public func getRequest(forKey key: String,
                           param: Dictionary<String, String>? = nil,
                           succeed: @escaping(Dictionary<String, Any>, Int, Bool)->(),
                           failure: @escaping(String)->()) {
        if let url = ConfigManager.default.getURLWithKey(key: key) {
            self.getRequest(forUrl: url, param: param, succeed: succeed, failure: failure)
        }
        
    }
    
    public func getRequest(forUrl url: String,
                           param: Dictionary<String, String>? = nil,
                           succeed: @escaping(Dictionary<String, Any>, Int, Bool)->(),
                           failure: @escaping(String)->()) {
        var finalUrl = url
        if let param = param {
            if !param.isEmpty {
                param.forEach { (key, value) in
                    finalUrl.append("?\(key)=\(value)")
                }
            }
        }
        self.request(type: .Get, url: finalUrl, succeed: succeed, failure: failure)
    }
    
    public func postRequest(forKey key: String,
                            param: Dictionary<String, Any>?,
                            succeed: @escaping(Dictionary<String, Any>, Int, Bool)->(),
                            failure: @escaping(String)->()) {
        if let url = ConfigManager.default.getURLWithKey(key: key) {
            self.postRequest(forUrl: url, param: param, succeed: succeed, failure: failure)
        }
    }
    
    public func postRequest(forUrl url: String,
                            param: Dictionary<String, Any>?,
                            succeed: @escaping(Dictionary<String, Any>, Int, Bool)->(),
                            failure: @escaping(String)->()) {
        self.request(type: .Post, url: url, parames: param, succeed: succeed, failure: failure)
    }
    
    
    func request(type: HTTPVerbType,
                 url: String,
                 parames: [String:Any]? = ["":"" as Any],
                 succeed: @escaping(Dictionary<String, Any>, Int, Bool)->(),
                 failure: @escaping(String)->()) {
        
        var method: HTTPMethod
        switch type {
        case .Get:
            method = .get
        case .Post:
            method = .post
        }
        let sessionURL = String.addQueryStringTo(URL:url, param: appededUrl)!
        DLog("sessionURL ====> \(sessionURL)")
        Alamofire.request(sessionURL, method:method, parameters:parames).responseJSON { (retultObject) in
            
            switch retultObject.result {
            //成功
            case .success:
                if let value = retultObject.result.value {
                    let dict = value as! Dictionary<String, Any>
                    // 成功闭包
                    let code = String(describing: dict["code"]!)
                    guard let codeNum = Int(code) else { return }
                    //增加判断，防止万一数据结构有变的崩溃
                    guard let json = dict["data"] as? Dictionary<String, Any> else {
                        succeed(dict, codeNum, false)
                        return
                    }
                    succeed(json, codeNum, true)
                }
            //失败
            case .failure:
                failure("failure")
            }
        }
    }
}
