//
//  RequestKey.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

/*****************************App非模块功能*****************************/

let kAppEnvInfoKey = "kAppEnvInfoKey";          // 环境key
let kAppBundleVersion = "kAppBundleVersion";    //缓存app版本的key，便于刚更新时区分当前版本与之前缓存的版本不一样
let kConfigDataUrlKey = "dataUrl";
//红点接口
let kCreditHotDot = "creditHotDot";

/*****************************账户相关*****************************/

//登录接口
let kKDLoginKey = "creditUserLogin";
//退出登录接口
let kKDLogoutKey = "creditUserLogout";
//快捷登录获取验证码
let kUserQuickLoginCode = "creditUserGetCodeLogin";
//快捷登录
let kUserQuickLoginBySms = "creditUserLoginBySms";
//设置手势密码拉取用户信息
let kUserHandPasswordGetUserInfo = "creditUserHandPasswordGetUserInfo";
//注册接口
let kKDZhuceKey = "creditUserRegister";
//注册验证码
let KUserRegGetCode = "creditUserRegGetCode";
//修改登录密码
let kUserChangePwd = "creditUserChangePwd";
//找回密码获取验证码
let kUserResetPwdCode = "creditUserResetPwdCode";
//获得未登录用户信息
let kUserState = "creditUserState";
//找回密码验证码
let kUserVerifyResetPwdCode = "creditUserResetPwdCode";
//找回密码验证个人信息
let kUserVerifyResetPassword = "creditUserVerifyResetPassword";
//找回登录密码设置新密码
let kUserResetPassword = "creditUserResetPassword";
//找回交易密码设置新密码
let kUserResetPayPassword = "creditUserResetPayPassword";
//初次设置交易密码
let kUserSetPaypassword = "creditUserSetPaypassword";
//修改交易密码
let kUserChangePaypassword = "creditUserChangePaypassword";
//我的借款
let kUserLoanGetMyOrders = "creditLoanGetMyOrders";
/***************************我的优惠**********************/
//现金券
let kMyCoupon = "creditMyCouponList";
//用户选择现金劵
let KChoosecash = "creditGetUserLoanList";
//确认现金劵抵扣
let kConfirmCash = "creditCouponDeductible";

//用户个人信息
let kUserGetInfo = "creditUserGetInfo";

//意见反馈
let kCreditInfoFeedback = "creditInfoFeedback";

/***************************首页**********************/
//首页
let kCreditAppIndex = "creditAppIndex";
let kCreditGetMallList = "creditGetMallList";
let kAnouncementList = "publicNoticeList"; //公告列表

let kUserInviteRedPackCount = "userInviteRedPackCount";//邀请红包汇总
let kUserInviteRedPackList = "userInviteRedPackList";   //邀请红包列表
let kUserInviteRedPackPopMoneyList = "userInviteRedPackPopMoneyList"; //红包提现记录
let kUserInviteRedPackPopMoney = "userInviteRedPackPopMoney";        //红包提现申请

let kCreditUserBaseProfile = "CreditUserBaseProfile";//用户信息
//userInviteRedPackList
//userInviteRedPackPopMoneyList
//userInviteRedPackPopMoney

let kCreditLoanAchieveLoan = "creditLoanAchieveLoan";

//新版首页
let kCreditAppIndexNew = "creditAppMultiIndex";
//首页激活弹框点击事件
let kCreditAppActiveShow = "creditAppActiveShow";
//首页弹框
let knoticePopBox  = "noticePopBox";
//启动页广告
let kNoticebounced = "creditNoticeStartPop";
//首页滚动页
let kNoticepopboxlist = "creditNoticePopList";


//红包接口
let kCreditIsExistCoupon = "creditIsExistCoupon";
//拒就赔红包
let kCreditUpdateRedPacket = "creditUpdateRedPacket";
/**************************认证**********************/
//认证列表
let kCreditCardGetVerificationInfo = "creditCardGetVerificationInfo";
//获取个人信息
let kCreditCardGetPersonInfo = "creditCardGetPersonInfo";
//保存个人信息
let kCreditCardSavePersonInfo = "creditCardSavePersonInfo";
//
let kCreditInfoSavePersonInfo = "creditInfoSavePersonInfo";
//获取工作信息
let kCreditCardGetWorkInfo = "creditCardGetWorkInfo";
//保存工作信息
let kCreditCardSaveWorkInfo = "creditCardSaveWorkInfo";
//获取紧急联系人信息
let kCreditCardGetContacts = "creditCardGetContacts";
//保存紧急联系人信息
let kCreditCardSaveContacts = "creditCardSaveContacts";

//获取芝麻参数
let kCreditZmMobileApi = "creditZmMobileApi";
//上报芝麻返回信息
let kZmMobileResultSave = "creditZmMobileResultSave";
//上传通讯录总数目
let kCreditInfoUploadContentsPrepared = "creditInfoUploadContentsPrepared";
//上传通讯录
let kInfoUpLoadContacts = "creditInfoUploadContents";
//上报app信息
let kReportKey = "creditAppDeviceReport";
/**************************支付宝爬虫**********************/
//上报支付宝爬虫信息
let kCrawlerKey = "creditInfoCaptureUpload";

//获取支付宝加密信息
let kCreditInfoCaptureInit = "creditInfoCaptureInit";

//照片
//获取照片列表
let kPictureGetPicList = "creditPictureGetPicList";
//上传照片列表
let kPictureUploadImage = "creditPictureUploadImage";

let kPictureUploadImg = "creditPictureUploadImg";


//获取银行卡列表
let kCreditCardBankCardInfo = "creditCardBankCardInfo";
//获取绑卡验证码
let kCreditCardGetCode = "creditCardGetCode";
//获取用户绑卡
let kCreditCardGetBankCard = "creditCardGetBankCard";
//绑卡
let kCreditCardAddBankCard = "creditCardAddBankCard";
//获取用户是否开通存管
let kCreditCardGetDepositOpenInfo = "creditCardGetDepositOpenInfo";
//2.7.0 - 银行卡列表
let kCreditCardCardList = "creditCardCardList";
//2.7.0 - 设置主卡
let kCreditCardSetMainCard = "creditCardSetMainCard";
//2.7.0 - 解除绑定
let kCreditCardUnbindCard = "creditCardUnbindCard";
//3.0.0 - 获取个人信息
let kCreditCardGetPersonAdditionInfo = "creditCardGetPersonAdditionInfo";
//3.0.0 - 修改个人信息
let kCreditCardSavePersonAdditionInfo = "creditCardSavePersonAdditionInfo";

/***************************申请借钱*******************/
//获取确认页面信息
let kCreditLoanGetConfirmLoan = "creditLoanGetConfirmLoan";
//获取确认页面信息(新)
let kCreditLoanGetConfirmLoan_new = "creditLoanGetConfirmLoanGai";
//申请借款
let kCreditLoanApplyLoan = "creditLoanApplyLoan";
//确认失败记录
let kCreditConfirmFailedLoan = "creditLoanConfirmFailedLoan";
//借款页面开通存管
let kCreditUserDepositOpen = "CreditUserDepositOpen";

/***************************提额**********************/
let kUserCenterPromote = "creditCardGetCardInfo";


/***************************还款**********************/
let kUserLoan = "creditLoanGetMyLoan";
//credit-loan/get-pay-order
let kCreditLoanGetPayOrder = "creditLoanGetPayOrder";
let kCreditLoanApplyRepay = "creditLoanApplyRepay";


let kCreditCardChangeCardCheck = "creditCardChangeCardCheck";
let kCreditCardChangeCard = "creditCardChangeCard";

/***************************上报用户位置信息**********************/
let KUserLocation = "creditInfoUploadLocation";
let KUserAppInfo  = "creditInfoUploadContents";

//认证中心获取额度
let kCreditAppUserCreditTop = "creditAppUserCreditTop";

//tabbar图片
let kCreditTabBarList = "creditTabBarList";
/***************************分享相关**********************/
//短信发送人数获取
let kCreditMessageashare = "creditCreditGetInviteLast";
//短信发送人数获取
let kCreditMessageaUp = "creditSendInviteSms";

//分享到社交网站计数统计
let kSocialMediaSiteVisitStasticsURLKey = "visit_stat_url";


//face++图片上报后台
let kCreditFacePlusIdcard = "FacePlusIdcard";

//百融SDK
let kBrSdk = "BrSdk";


let kTestID = 48;

/******************************公告******************************/
//['1'=>'借款首页','2'=>'借款详情页','3'=>'还款详情页','4'=>'我的页面','5'=>'提现页面'];
let kLoanHomeAnouncementParam = "1";
let kLoanDetailAnouncementParam = "2";
let kPaymentDetailAnouncementParam = "3";
let kMineAnouncementParam = "4";
let kWithdrawAnouncementParam = "5";

//['1'=>'借款首页','2'=>'借款详情页','3'=>'还款详情页','4'=>'我的页面','5'=>'提现页面']

let kHomeAnouceStatuskey = "homeAnouceStatus";
let kLoanDetailAnouceStatuskey = "loanDetailAnouceStatus";//kLoanDetailAnouncementParam
let kPaymentDetailAnouceStatuskey = "paymentDetailAnouceStatus";
let kMineAnouceStatuskey = "mineAnouceStatus";
let kWithdrawAnouceStatuskey = "withdrawAnouceStatus";

let kCreditAppRandom = "creditAppRandom";

let kDiscoverGetInfo = "discoverGetInfo";
let kDiscoverQuotaLog = "discoverQuotaLog";

let kBqsSaveToken = "BqsSaveToken";

let kCreditCardGuide = "creditCardGuide";

let kCreditAppAppIndex = "creditAppAppIndex";

let kCreditAppAddAppJurisdiction = "creditAppAddAppJurisdiction";

let kCreditCardSaveZmxy = "creditCardSaveZmxy";

let kCreditLoanGetVipDialog = "creditLoanGetVipDialog";
