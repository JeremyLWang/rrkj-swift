//
//  ProjectConst.swift
//  rrkj
//
//  Created by Jeremy on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

/************************************  UI相关  ************************************/
//颜色相关
let BUTTONGRAY = "#d9d9d9";
let YGBGREY = "#eff3f5";
let YGBPickerGREY = "#f4f4f6";
let projectShortName = "rrkj"

/************************************  项目全局相关  ************************************/
let kAppLocalVersion = "app_local_version";
let kFirstInApp = "firstInApp";
let kDebugDomainName = ".jisuqianbao.com";
let kReleaseDomainName = "kj.hualiwangluo.com";
let kReleaseURL = "https://kj.hualiwangluo.com/credit/web/credit-app/config";
let kOfficialWebsite = "http://www.hualiwangluo.com/"; //官网链接
let kSessionID = "SESSIONID";
let kCookieData = "Cookie_Data";
let kUserAgentResult = "uaResult";
let kPullRefreshUIDefaultTitle = "信用让生活更美好";

/************************************  通知相关  ************************************/
let kCustomVipAlertClosedNoti = "kCustomVipAlertClosedNoti";
let kUserLogOutNotiKey = "loginOut";
let kUserLoginSuccessNotiKey = "loginSuccess";

/************************************  第三方集成相关  ************************************/

//高德地图APIkey--类型设置为WEB
let kAmapApiKey = "42ebf64dce1c7087eb4252ee42633c67";

let kAppMarket = "AppStore";

let kAppIDKey = "1373573317";

let kSchemeKey = "schemerrkj";

let kUmengAppKey = "5ac9bc1da40fa369c7000109";

//极光推送相关
let kJpushAppKey = "86c6555584483d3b7eb89d17";
let kJpushMasterSecret = "1624aa459f3d58e6a0cfaab6";

//BugTags相关
//368f1e94a8fcdc4622264ea21753d6e7  beta
//ce8f955a075db45db1b86c81a1a04a30  live
let kBugTagsAppKey = "368f1e94a8fcdc4622264ea21753d6e7";

/**
 *  微信分享相关信息 wx5bd3a8d6a4b55b43 | 59ce146391299f39a26677b9dd06c39e已更改
 */
let kWxAppKey = "wx27c28d31949ec20a";
let kWxAppSecret = "78d5c29648ccbcb25bb7d19369cd7b97";

/**
 *  QQ分享相关信息
 */
let kQQ_URI = "http://kj.hualiwangluo.com";
let kQQAppID = "1106743681";
let kQQAppKey = "LpWtfyqOPphQgOdy";
