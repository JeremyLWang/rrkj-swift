//
//  GlobalFunc.swift
//  rrkj
//
//  Created by Jeremy Wang on 2018/5/9.
//  Copyright © 2018 Jeremy Wang. All rights reserved.
//

func delay(_ seconds: Double, completion: @escaping ()->Void) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
}

/**
 * log 在release 版本不打印
 * `Other Swift Flags`，修改debug模式的flag 为“D”
 */
func DLog(_ item: @autoclosure () -> Any) {
    if isDebug {
        print(item())
    }
}

//是否为debug模式
var isDebug: Bool {
    get {
        #if DEBUG
        return true
        #else
        return false
        #endif
    }
}

func toastShow(text: String) {
    UIApplication.shared.keyWindow?.makeToast(text, position: .center)
}


func tapToast(text: String, action: @escaping(Bool)->Void) {
    UIApplication.shared.keyWindow?.makeToast(text, position: .center){ (didTap) in
        action(didTap)
    }
}


